//==========================================
// Initial Author: Dimitrios Karasavvas
// Date:   29 Jan 2021
//==========================================



static char input_files[][800] ={

    "/eos/user/a/adlintul/scouting/ntuples/PUpmx_112X_mcRun3_2021_realistic_v13_forPMX-v1/RelValQCD_Pt_1800_2400_14/default_trackcleaningcut20_chisqrPtErrorCut5/21-05-2021-1208/hadd_ntuple.root",
    "/eos/user/a/adlintul/scouting/ntuples/PUpmx_112X_mcRun3_2021_realistic_v13_forPMX-v1/RelValQCD_Pt_1800_2400_14/default_trackcleaningcut20_chisqrPtErrorCut10/20-05-2021-1644/hadd_ntuple.root",
    "/eos/user/a/adlintul/scouting/ntuples/PUpmx_112X_mcRun3_2021_realistic_v13_forPMX-v1/RelValQCD_Pt_1800_2400_14/default_trackcleaningcut20_chisqrPtErrorCut20/20-05-2021-1643/hadd_ntuple.root",
    "/eos/user/a/adlintul/scouting/ntuples/PUpmx_112X_mcRun3_2021_realistic_v13_forPMX-v1/RelValQCD_Pt_1800_2400_14/default_trackcleaningcut20_chisqrPtErrorCut50/20-05-2021-1646/hadd_ntuple.root",
    "/eos/user/a/adlintul/scouting/ntuples/PUpmx_112X_mcRun3_2021_realistic_v13_forPMX-v1/RelValQCD_Pt_1800_2400_14/default_trackcleaningcut20_chisqrPtErrorCut100/21-05-2021-0824/hadd_ntuple.root",

    // "/eos/user/a/adlintul/scouting/ntuples/PUpmx_112X_mcRun3_2021_realistic_v13_forPMX-v1/RelValQCD_Pt_1800_2400_14/default/07-05-2021-1104/hadd_ntuple.root",
    // "/eos/user/a/adlintul/scouting/ntuples/PUpmx_112X_mcRun3_2021_realistic_v13_forPMX-v1/RelValQCD_Pt_1800_2400_14/default_ptErrorCut100/18-05-2021-1453/hadd_ntuple.root",
    // "/eos/user/a/adlintul/scouting/ntuples/PUpmx_112X_mcRun3_2021_realistic_v13_forPMX-v1/RelValQCD_Pt_1800_2400_14/default_trackcleaningcut/18-05-2021-1029/hadd_ntuple.root",
    // "/eos/user/a/adlintul/scouting/ntuples/PUpmx_112X_mcRun3_2021_realistic_v13_forPMX-v1/RelValQCD_Pt_1800_2400_14/default_trackcleaningcut10/19-05-2021-1544/hadd_ntuple.root",
    // "/eos/user/a/adlintul/scouting/ntuples/PUpmx_112X_mcRun3_2021_realistic_v13_forPMX-v1/RelValQCD_Pt_1800_2400_14/default_trackcleaningcut15/19-05-2021-1543/hadd_ntuple.root",
    "/eos/user/a/adlintul/scouting/ntuples/PUpmx_112X_mcRun3_2021_realistic_v13_forPMX-v1/RelValQCD_Pt_1800_2400_14/default_trackcleaningcut20/19-05-2021-1319/hadd_ntuple.root",
    // "/eos/user/a/adlintul/scouting/ntuples/PUpmx_112X_mcRun3_2021_realistic_v13_forPMX-v1/RelValQCD_Pt_1800_2400_14/default_trackcleaningcut30/19-05-2021-1322/hadd_ntuple.root",
    // "/eos/user/a/adlintul/scouting/ntuples/PUpmx_112X_mcRun3_2021_realistic_v13_forPMX-v1/RelValQCD_Pt_1800_2400_14/default_trackcleaningcut40/19-05-2021-1320/hadd_ntuple.root",
    // "/eos/user/a/adlintul/scouting/ntuples/PUpmx_112X_mcRun3_2021_realistic_v13_forPMX-v1/RelValQCD_Pt_1800_2400_14/default_trackcleaningcut100/19-05-2021-1039/hadd_ntuple.root",
    // "/eos/user/a/adlintul/scouting/ntuples/PUpmx_112X_mcRun3_2021_realistic_v13_forPMX-v1/RelValQCD_Pt_1800_2400_14/default_trackcleaningcut150/19-05-2021-1149/hadd_ntuple.root",
    // "/eos/user/a/adlintul/scouting/ntuples/PU/eos/user/a/adlintul/scouting/ntuples/PUpmx_112X_mcRun3_2021_realistic_v13_forPMX-v1/RelValQCD_Pt_1800_2400_14/pmx_112X_mcRun3_2021_realistic_v13_forPMX-v1/RelValQCD_Pt_1800_2400_14/default_trackcleaningcut200/19-05-2021-1240/hadd_ntuple.root",
    "/eos/user/a/adlintul/scouting/ntuples/PUpmx_112X_mcRun3_2021_realistic_v13_forPMX-v1/RelValQCD_Pt_1800_2400_14/default_chisqr_ptErrorCut5/18-05-2021-1256/hadd_ntuple.root",
    // "/eos/user/a/adlintul/scouting/ntuples/PUpmx_112X_mcRun3_2021_realistic_v13_forPMX-v1/RelValQCD_Pt_1800_2400_14/default_chisqr_ptErrorCut10/18-05-2021-1301/hadd_ntuple.root",
    // "/eos/user/a/adlintul/scouting/ntuples/PUpmx_112X_mcRun3_2021_realistic_v13_forPMX-v1/RelValQCD_Pt_1800_2400_14/default_chisqr_ptErrorCut20/18-05-2021-1314/hadd_ntuple.root",
    // "/eos/user/a/adlintul/scouting/ntuples/PUpmx_112X_mcRun3_2021_realistic_v13_forPMX-v1/RelValQCD_Pt_1800_2400_14/default_chisqr_ptErrorCut100/18-05-2021-1422/hadd_ntuple.root",
    "/eos/user/a/adlintul/scouting/ntuples/PUpmx_112X_mcRun3_2021_realistic_v13_forPMX-v1/RelValQCD_Pt_1800_2400_14/adriano_tracks_ptErrorCut5/07-05-2021-1041/hadd_ntuple.root",
    // "/eos/user/a/adlintul/scouting/ntuples/PUpmx_112X_mcRun3_2021_realistic_v13_forPMX-v1/RelValQCD_Pt_1800_2400_14/adriano_tracks_ptErrorCut10/07-05-2021-1021/hadd_ntuple.root",
    // "/eos/user/a/adlintul/scouting/ntuples/PUpmx_112X_mcRun3_2021_realistic_v13_forPMX-v1/RelValQCD_Pt_1800_2400_14/full_tracks_new/12-05-2021-1710/hadd_ntuple.root",

// "/eos/cms/store/group/phys_exotica/dijet/Dijet13TeV/dimitris/ScoutingQCDtoBeTransfered/IncludingMet/FullTracking/FullTracking_QCD_reclusteredAK8.root",
// "/eos/cms/store/group/phys_exotica/dijet/Dijet13TeV/dimitris/ScoutingQCDtoBeTransfered/IncludingMet/Patatrack_tightCuts/Patatrack_QCD_reclusteredAK8_tightCuts.root",
// "/eos/cms/store/group/phys_exotica/dijet/Dijet13TeV/dimitris/ScoutingQCDtoBeTransfered/IncludingMet/Patatrack_looseCuts/Patatrack_QCD_reclusteredAK8_looseCuts.root"

};

static char analyzer_path[500] = {"/afs/cern.ch/user/a/adlintul/2021/scouting/reana/pfpatatrackvalidation/ProducerTest"}; 
static char output_directory[500] = {"output/PUpmx_112X_mcRun3_2021_realistic_v13_forPMX-v1/RelValQCD_Pt_1800_2400_14/test/pf_candidate_characteristic"}; 
static char image_name[200] = {"RelValQCD_Pt_1800_2400_14"}; 


// static char legend_array[][500] = { "FullTracking" ,  "PatatrackTight", "PatatrackLoose"  };
//static char legend_array[][500] = { "Default", "Default_ptECut100" "Default_cleanCut", "Default_chisqr_ptECut5", "Default_chisqr_ptECut10", "Default_chisqr_ptECut20", "Default_chisqr_ptECut100" ,"Adriano_ptErrorCut5", "Adriano_ptErrorCut10", "Full_tracking" };
// static char legend_array[][500] = { "Def_CleanCut20_ptECut5, Def_CleanCut20_Chi2Cut10", "Def_CleanCut20_Chi2Cut15", "Def_CleanCut20_Chi2Cut20", "Def_CleanCut20_Chi2Cut50", "Adriano" };
static char legend_array[][500] = { "CleanCut20_Chi2Cut5", "CleanCut20_Chi2Cut10", "CleanCut20_Chi2Cut20", "CleanCut20_Chi2Cut50", "CleanCut20_Chi2Cut100", "CleanCut20", "Chi2Cut5", "Adriano" };


static double yBnd[]={0.0, 1.3, 2.4, 2.7, 3.0};  


static double ptBnd[] = {30.,   70., 110., 150. , 190. , 230., 270., 310., 350.,  410., 470., 530. , 590. , 650., 710., 770., 830.,   900., 1000., 1150. , 1300. , 1550., 1850., 2100., 3000.}; //All pT regime //this is only used for the jet pT resolution calculations 


static double pTlowCut  = 30; //cuts on jet pT's to be considered
static double pThighCut = 3000;

static bool useWeights  = true;
static bool useGausFits = true; //only used for jet pT resolution calculations
static bool useRecoBins = true; //calc resolution in reco or gen pT bins


static int EventsToProcess = 6000000; //negative value will process all events in each tree

static double DR_threshold = 0.2; //used for gen-to-reco matching


// ===================================================== Plotting options ==============================================================
static bool plot_matched = true; // choose to plot btb or matched 
static bool scale_histos = true ; //scale all histos to the number of entries of the histo of the 1st input
static bool Save_Plots = true; 


static int Colors[] = { 1, 4, 2 , 6, 3, 7 , 28, 46} ; // black, blue, red , magenta, green, light blue ,  brown, redish. // define colors for each input
static int MarkerStyle[] = { 8, 2, 5 , 4, 22, 21, 27, 28 } ; // 


static int PadColumnsEtaBins = 3;  // define number of pad columns for the canvas
static int PadRowsEtaBins = 2;     // define number of pad rows for the canvas

//for nicely looking plots it should be : eta_bins <= PadColumnsEtaBins * PadRowsEtaBins  

static int Canvas_XpixelsEtaBins = PadColumnsEtaBins*333; //define canvas X pixels
static int Canvas_YpixelsEtaBins = PadRowsEtaBins*500;    //define canvas Y pixels

static double YaxisLowEndMultiplier = 0.00005; // define lower bound of Y axis for plot: Lower Bound = YaxisLowEndMultiplier* histo_maximum
static double YaxisHighEndMultiplier = 10.;    // define upper bound of Y axis for plot: Upper Bound = YaxisHighEndMultiplier* histo_maximum


//==============================additional options only used for the jet pT resolution & response plots ===============================

static int PadColumnsPtBins = 5; 
static int PadRowsPtBins = 5;

//static int Canvas_XpixelsPtBins = PadColumnsPtBins*333;
//static int Canvas_YpixelsPtBins = PadRowsPtBins*500;

static int Canvas_XpixelsPtBins = PadColumnsPtBins*450;
static int Canvas_YpixelsPtBins = PadRowsPtBins*450;













